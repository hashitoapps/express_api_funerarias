var models     = require('../../models');
var repository = require('../../repositories/Usuario');
var controller = require('./BaseController')(repository);
var smtpTransport = require('nodemailer-smtp-transport');
var nodemailer    = require('nodemailer');
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'carlosmaroz@gmail.com', // my mail
        pass: 'carlosmaroz123'
    }
}));
var semilla = "jodasent";
var mcrypt = require('mcrypt');
var MCrypt = require('mcrypt').MCrypt;

controller.Login = function(req, res) {
  console.log(req.body);
  console.log("ISFACEBOOK:"+req.body.ISFACEBOOK);
  models
    .Usuario
    .Login(req.body.Correo, req.body.Clave,req.body.ISFACEBOOK)
    .then(function(result) {
      res.send({
        Token: result
      });
    })
    .catch(function(error) {
      console.log(error);

      res.status(500).send(error);
    });
};


controller.Signup = function(req, res) {
  repository
    .Insert(req.body)
    .then(function(result) {
      return models
        .Usuario
        .Login(req.body.Correo, req.body.Clave);
    })
    .then(function(result) {
      res.send({
        Token: result
      });
    })
    .catch(function(error) {
      console.log(error);

      res.status(500).send(error);
    });
};

controller.Facebook = function(req, res) {
  var reqBody    = req.body;
  var request    = require('request');
  var urlRequest = 'https://graph.facebook.com/v2.7/me?fields=email,name,id,picture&access_token=' + reqBody.access_token;

  request(urlRequest, function (error, response, body) {
    console.log("body"+body);

    if (!error && response.statusCode == 200) {
      var fbInfo = JSON.parse(body);

      fbInfo.Clave     = fbInfo.id;
      fbInfo.Nombre    = fbInfo.name;
      fbInfo.Avatar    = fbInfo.picture.data.url;
      fbInfo.Proveedor = reqBody.Proveedor;
      fbInfo.PaiId     = 47;//colombia

      if(!fbInfo.email){
        fbInfo.email = [fbInfo.id,'@','facebook.com'].join('');
      }

      models
        .Usuario
        .findOrCreate({
          include: [models.Pais],
          where: {Correo: fbInfo.email},
          defaults: fbInfo
        })
        /*.then(function(result) {
          return result[0].updateAttributes(fbInfo);
        })*/
        .then(function(result) {
          res.send(result[0]);
        })
        .catch(function(error) {
          console.log(error);
          res.status(500).send(error);
        });
    } else {
      res.status(500).send(error);
    }
  });
};

controller.GuardarAvatar2 = function(req,res){

  /*var id    = req.params.Id; //usuarioId

  console.log(req.body.usr);

  var base64Data = req.body.imagen.replace(/^data:image\/png;base64,/, "");

  var fs          = require('fs');

  var producto    = req.params.Id;
  var nPath       = 'public/avatars/' + producto + '/';
  if (!fs.existsSync(nPath)) {
    fs.mkdirSync(nPath);
  }


  var extensiones = [
    {ctype: 'image/jpeg',  ext: 'jpg'},
    {ctype: 'image/pjpeg', ext: 'jpeg'},
    {ctype: 'image/png',   ext: 'png'},
    {ctype: 'image/bmp',   ext: 'bmp'}
  ];



    var newPath = [
      nPath,
      generateUUID(),
      '.jpg',
      ].join('');

    require("fs").writeFile(newPath, base64Data, 'base64', function(err) {
      console.log(err);
    });

    newPath = [
            [req.protocol, req.hostname].join('://'),
            newPath.replace('public/', '')
          ].join('/');

    req.body.Avatar=newPath;

          repository
            .Update(id,req.body)
            .then(function(result) {
              res.send(result);
            })
            .catch(function(error) {
              console.log(error);

              res.status(500).send(error);
            });*/


    //---------------

    var base64Data = req.body.imagen.replace("data:image\/*;charset=utf-8;base64,", "");

    var fs          = require('fs');

    //var file        = req.files.file;
    var producto    = req.params.Id;
    var nPath       = 'public/avatars/' + producto + '/';
    if (!fs.existsSync(nPath)) {
      // Do something
      fs.mkdirSync(nPath);
    }


    var extensiones = [
      {ctype: 'image/jpeg',  ext: 'jpg'},
      {ctype: 'image/pjpeg', ext: 'jpeg'},
      {ctype: 'image/png',   ext: 'png'},
      {ctype: 'image/bmp',   ext: 'bmp'}
    ];

      var newPath = [
        nPath,
        generateUUID(),
        '.jpg',
        ].join('');

      require("fs").writeFile(newPath, base64Data, 'base64', function(err) {
        console.log(err);
      });

      newPath = [
              [req.protocol, req.hostname].join('://'),
              newPath.replace('public/', '')
            ].join('/');


              req.body.Avatar =newPath;

              repository
                .Update(producto,req.body)
                .then(function(result) {
                  res.send(result);
                })
                .catch(function(error) {
                  console.log(error);

                  res.status(500).send(error);
                });

}

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};



//criferlo
//se desarrolla GuardarAvatar para reemplazar la funcion GuardarImagen
controller.GuardarAvatar = function(req,res){
  var fs          = require('fs');
  var file        = req.files.file;
  var id    = req.params.Id; //usuarioId
  var nPath       = 'public/avatars/' + id;
  var extensiones = [
    {ctype: 'image/jpeg',  ext: 'jpg'},
    {ctype: 'image/pjpeg', ext: 'jpeg'},
    {ctype: 'image/png',   ext: 'png'},
    {ctype: 'image/bmp',   ext: 'bmp'}
  ];

  var extension = extensiones.find(function(ext) {
    return ext.ctype == file.headers['content-type'];
  });

  if (extension) {
    var path    = file.path;
    var newPath = [
      nPath,
      //generateUUID(),
      '.',
      extension.ext].join('');

    fs.mkdir(nPath, function(e) {
      var is      = fs.createReadStream(path);
      var os      = fs.createWriteStream(newPath);

      is.pipe(os);

      is.on('end', function() {
        try {
          //eliminamos el archivo temporal
          fs.unlinkSync(path);

          newPath = [
            [req.protocol, req.hostname].join('://'),
            newPath.replace('public/', '')
          ].join('/');

          console.log("nuevo path:"+newPath);

          //criferlo devuelve datos
          //res.send(res);

          req.body.Avatar=newPath;

          repository
            .Update(id,req.body)
            .then(function(result) {
              res.send(result);
            })
            .catch(function(error) {
              console.log(error);

              res.status(500).send(error);
            });

          /*repositoryImg
            .Insert({
              RutaGrande: newPath,
              RutaPequeño: newPath,
              ProductoId: producto
            })
            .then(function(result) {
              res.send(result);
            })
            .catch(function(error) {
              console.log(error);
              res.status(500).send(error);
            })*/


        } catch (err) {
          res.status(500).send('error al cargar archivo ' + err);
        }
      });
    });
  } else {
    res.status(500).send([
      'Formato de archivo incorrecto,',
      'debe de cargar una imagen'
    ].join(' '));
  }


};

controller.GuardarImagen = function(req, res) {
  var base64  = require('base64-img');

  base64.img(
    req.body.Avatar,
    'public/avatars',
    req.params.email,
    function(error, filePath) {
      if (error) {
        console.log(error);
        return res.status(500).send(error);
      }

      filePath = filePath.replace('public/', '');

      console.log(filePath);

      res.send(filePath);
    });



  // var fs          = require('fs');
  // var file        = req.files.file;
  // var email       = req.params.email;
  // var nPath       = 'public/avatars/';
  // var extensiones = [
  //   {ctype: 'image/jpeg',  ext: 'jpg'},
  //   {ctype: 'image/pjpeg', ext: 'jpeg'},
  //   {ctype: 'image/png',   ext: 'png'},
  //   {ctype: 'image/bmp',   ext: 'bmp'}
  // ];

  // console.log(file);
  // console.log(email);

  // var extension = extensiones.find(function(ext) {
  //   return ext.ctype == file.headers['content-type'];
  // });

  // console.log(file.headers['content-type']);
  // console.log(extension);

  // if (extension) {
  //   var path    = file.path;
  //   var newPath = [
  //     nPath,
  //     email,
  //     '.',
  //     extension.ext].join('');
  //   var is      = fs.createReadStream(path);
  //   var os      = fs.createWriteStream(newPath);

  //   is.pipe(os);

  //   is.on('end', function() {
  //     try {
  //       //eliminamos el archivo temporal
  //       fs.unlinkSync(path);

  //       console.log('Nueva ruta');
  //       console.log(newPath);

  //       res.send(newPath);
  //     } catch (err) {
  //       res.status(500).send('error al cargar archivo ' + err);
  //     }
  //   });
  // } else {
  //   res.status(500).send([
  //     'Formato de archivo incorrecto,',
  //     'debe de cargar una imagen'
  //   ].join(' '));
  // }
};


controller.RecuperarContra = function(req,res){
  var email = req.body.email;
  console.log("correo",email);

  //var algos = mcrypt.getAlgorithmNames();
  //console.log(algos);




  //console.log("encriptado:"+encriptado);

  //var plaintext = desEcb.decrypt(new Buffer(encriptado, 'base64'));
  //console.log("desenscriiptado"+plaintext.toString());

  //res.status(200).send("ok");

  repository.LoadByEmail(email)
    .then(function(user){
        if(user){
          //envio de correo
          console.log("encuentra al usuario");

            var mailOptions = {
              from: 'Grupo Prevenir Express <clientes@prevenirexpress.com>',
              to: email,
              subject: ['Recuperación de contraseña'].join(' ')
            };

            //encripto el correo
            var desEcb = new MCrypt('des', 'ecb');
            desEcb.open(semilla);
            var ciphertext = desEcb.encrypt(email);
            var encriptado = ciphertext.toString('base64');

            console.log("enc:"+encriptado);

            var cad1 = "<h1><i> Recupera la contraseña en este enlace:</h2>";
            //var cad2 = "<a href='http://localhost/express_web/#/usuario/";
            var cad2 = "<a href='http://www.prevenirexpress.com/#/usuario/"
            var cad3 = "'>Recuperar</a>";
            var send = cad1.concat(cad2,'nuevacontra/',encriptado,cad3);
            console.log(send);
            mailOptions.html = send;

            transporter.sendMail(mailOptions, function(error, info) {

              if (error) {
                console.log(error);
              } else {
                console.log('Message sent: ' + info.response);
              }

              res.status(200).send(info.response);

            });

        }else{
          res.status(404).send("Usuario no encontrado");
        }
    })
    .catch(function(err){
      var err = { error: err};
      res.status(500).send(err);
    })

}

controller.ObtenerDesencriptado = function(req,res){
    //encripto el correo
    var desEcb = new MCrypt('des', 'ecb');
    desEcb.open(semilla);
    var ciphertext = desEcb.decrypt(new Buffer(req.body.email, 'base64'));
    var desencriptado = ciphertext.toString();
    var data  = {data: desencriptado};
    res.status(200).send(data);
}

controller.ConsultarPorEmail = function(req,res){
    var email = req.body.email;
    console.log("correo",email);
    email = email.replace("\0\0\0","");
    repository.LoadByEmail(email)
    .then(function(user){
        if(user){
           res.status(200).send(user);
        }else{
           var nohay = {data: "No hay usuarios con el correo mencionado."};
           res.status(500).send(nohay);
        }
    })
    .catch(function(err){
      console.log("error:"+err);
    })

}



module.exports = controller;
