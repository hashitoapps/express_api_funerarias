var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var includes   = [{
    model: models.Imagen
  }, {
    model: models.Usuario
  }, {
    model: models.Variedad
  },{
    model: models.ProductoMunicipio
  }];
var repository = Repository(models.Producto, includes);

repository.GetProductAddVisit = function(Id) {
  return new Promise(function(resolve, reject) {
    models.Producto
      .find({
        where: {
          Id: Id,
          Estado:1
        },
        include: includes
      })
      .then(function(model) {
        console.log('Número de visitas: '  + (model.Visitas + 1));
        model.updateAttributes({Visitas: model.Visitas + 1});

        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.GetProductsByState = function(Estado) {
  return new Promise(function(resolve, reject) {
    models.Producto
      .find({
        where: {Estado: Estado},
        include: includes
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.LoadSimilar = function(Id, pageSize) {
  return this.entity
    .findAll({
      offset: 0,
      limit: pageSize,
      order: 'MeGusta DESC',
      include: this.includes
    });
};

repository.LoadPageByQuery2 = function(query, page, pageSize, orderBy, sortOrder,municipioId) {
  console.log("entra aqui----------",query);
  return this.entity
    .findAndCountAll({
      where: query,
      offset: (page - 1) * pageSize,
      limit: pageSize,
      order: [orderBy, sortOrder].join(' '),
      include: [{
          model: models.Imagen
        }, {
          model: models.Usuario
        }, {
          model: models.Variedad
        },{
          model: models.ProductoMunicipio ,  where: {MunicipioId: municipioId }
        }]
    });
};


repository.Validate = function(model) {
  var error = null;

  if (model.Nombre === undefined || model.Nombre === null) {
    error = 'Debe de ingresar un nombre válido de producto';
  }
  /*if (model.DescripcionCorta === undefined || model.DescripcionCorta === null) {
    error = 'Debe de ingresar una descripción corta válida';
  }*/
  if (model.DescripcionLarga === undefined || model.DescripcionLarga === null) {
    error = 'Debe de ingresar una descripción larga válida';
  }
  /*if (model.Precio === undefined || model.Precio === null) {
    error = 'Debe de ingresar un precio válido';
  }
  if (model.PorcentajePromo === undefined || model.PorcentajePromo === null) {
    error = 'Debe de ingresar un porcentaje de promoción válido';
  }*/
  if (model.TipoProducto === undefined || model.TipoProducto === null) {
    error = 'Debe de ingresar un tipo de producto válido';
  }

  if (model.ImagenPrincipal === undefined || model.ImagenPrincipal === null) {
    model.ImagenPrincipal = '';
  }

  if (model.Visitas === undefined || model.Visitas === null) {
    model.Visitas = 0;
  }

  if (model.MeGusta === undefined || model.MeGusta === null) {
    model.MeGusta = 0;
  }

  if (model.Estado === undefined || model.Estado === null) {
    model.Estado = 0;
  }

  return error;
}

module.exports = repository;
