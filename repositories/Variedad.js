var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Variedad, []);

repository.Validate = function(model) {
  var error = null;

  //Bloque que valida los errores
  if (model.Nombre === undefined ||
      model.Nombre === null) {
    error = 'Debe de proporcionar un nombre de variedad valido';
  }

  return error;
};

module.exports = repository;
