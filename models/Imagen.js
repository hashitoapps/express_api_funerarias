'use strict';

//-------------------------------------------------------------------------
// ENTIDAD  Imagen
//-------------------------------------------------------------------------

module.exports = function(sequelize, DataTypes) {
  var Imagen = sequelize.define('Imagen', {
    Id: {type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
    RutaGrande: {type: DataTypes.STRING, allowNull: false},
    RutaPequeño: {type: DataTypes.STRING, allowNull: false}
  }, {
    classMethods: {
      associate: function(models) {
        Imagen.belongsTo(models.Producto);
      }
    }
  });

  return Imagen;
};
