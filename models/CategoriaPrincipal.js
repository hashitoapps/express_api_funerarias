"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  CategoriaPrincipal
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var CategoriaPrincipal = sequelize.define('CategoriaPrincipal', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},		
		Nombre: { type: DataTypes.STRING, allowNull: false},		
	}, {
		classMethods: {
			associate: function (models) {
				CategoriaPrincipal.hasMany(models.SubCategoria);
			}
		}
	});

	return CategoriaPrincipal;
};