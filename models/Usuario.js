'use strict';

var Utils              = {};
var sequelize          = require('sequelize');
var jwt                = require('jwt-simple');
var moment             = require('moment');
var bcrypt             = sequelize.Promise.promisifyAll(require('bcryptjs'));
var SALT_WORK_FACTOR   = 10;
var MAX_LOGIN_ATTEMPTS = 5;
var LOCK_TIME          = 2 * 60 * 60 * 1000;

Utils.HashPassword = function(user, options) {
  if (user._changed && user._changed.Clave) {
    return bcrypt
      .genSaltAsync(SALT_WORK_FACTOR)
      .then(function(salt) {
        return bcrypt
          .hashAsync(user.Clave, salt)
      })
      .then(function(hash) {
        user.Clave = hash;

        return sequelize.Promise.resolve(user);
      });
  }
};

Utils.Login = function(EMAIL, Clave,ISFACEBOOK) {
  var models = require('../models');
  var User   = null;

  return models
    .Usuario
    .find({
      attributes: ['Id', 'Correo','Avatar','Identificacion','Descripcion','Nombre','Direccion',
      'Telefono','TelefonoWhatsApp','Clave','LAST_LOGIN','LOCKED','Proveedor','Completo','Ubicacion',
      'createdAt','updatedAt','UrlPagina','NombreEmpresa','Nit','PaiId'],
      include: [models.Pais],
      where: {
        Correo: EMAIL
      }
    })
    .then(function(user) {
      if (!user) {
        return sequelize.Promise.reject('El usuario no existe.');
      }

      if (user.LOCKED) {
        return sequelize.Promise.reject('El usuario se encuentra bloqueado.');
      }

      User = user;

      return ISFACEBOOK;

      /*if(!ISFACEBOOK){

      }else{

      }*/

    })
    .then(function(isFacebook) {

      if(!isFacebook){
        return User.ComparePassword(Clave);
      }else{
        return true;
      }
    })
    .then(function(isMatch){

      if (!isMatch) {
        return sequelize.Promise.reject('Clave incorrecta.');
      }

      //return User;
      return User.updateAttributes({
        LAST_LOGIN: new Date(),
        LOCKED: false
      });

    })
    .then(function(user) {
      console.log("---------user antes de token"+user);
      user.Clave = undefined;
      return sequelize.Promise.resolve(Utils.CreateToken(user));
    });
};

Utils.LoginMobile = function(EMAIL) {
  var models = require('../models');
  var User   = null;

  return models
    .Usuario
    .find({
      where: {
        Correo: EMAIL
      }
    })
    .then(function(user) {
      if (!user) {
        return sequelize.Promise.reject('El usuario no existe.');
      }

      if (user.LOCKED) {
        return sequelize.Promise.reject('El usuario se encuentra bloqueado.');
      }

      User = user;

      return User.updateAttributes({
        LAST_LOGIN: new Date(),
        LOCKED: false
      });
    })
    .then(function(user) {
      user.Clave = undefined;
      return sequelize.Promise.resolve(Utils.CreateToken(user));
    });
};


Utils.ComparePassword = function(password) {
  return bcrypt
    .compareAsync(password, this.Clave);
};

Utils.CreateToken = function(user) {
  var config = require('../config/config-jwt');
  var payload = {
    sub: user,
    iat: moment().unix(),
    exp: moment().add(10, 'years').unix(),
  };

  return jwt.encode(payload, config.TOKEN_SECRET);
};

//-------------------------------------------------------------------------
// ENTIDAD  Usuario
//-------------------------------------------------------------------------

module.exports = function(sequelize, DataTypes) {

  var Usuario = sequelize.define('Usuario', {
    Id: {type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
    Correo: {type:DataTypes.STRING, allowNull: false, unique: true},
    Avatar: {type:DataTypes.STRING, allowNull: true, default: 'http://api.prevenirexpress.com/avatar.png'},
    Identificacion: {type:DataTypes.TEXT, allowNull: true},
    Descripcion: {type:DataTypes.TEXT, allowNull: true},
    Nombre: {type: DataTypes.STRING, allowNull: false},
    Direccion: {type:DataTypes.STRING, allowNull: true},
    Telefono: {type:DataTypes.STRING, allowNull: true},
    TelefonoWhatsApp: {type:DataTypes.STRING, allowNull: true},
    Clave: {type:DataTypes.STRING, allowNull: false},
    LAST_LOGIN: {type: DataTypes.DATE, allowNull: true},
    LOCKED: {type: DataTypes.BOOLEAN, allowNull: true},
    Proveedor: {type: DataTypes.BOOLEAN, allowNull: true},
    Completo: {type: DataTypes.BOOLEAN, allowNull: true, default: false},
    UrlIntroduccion: {type:DataTypes.TEXT, allowNull: true},
    Ubicacion: {type:DataTypes.TEXT, allowNull:true},
    UrlPagina: {type: DataTypes.TEXT, allowNull: true},
    NombreEmpresa: {type: DataTypes.TEXT, allowNull: true},
    Nit: {type: DataTypes.TEXT, allowNull:true},
    UsuarioCitasId: {type: DataTypes.INTEGER, allowNull: true}
  }, {
    hooks: {
      beforeCreate: Utils.HashPassword,
      beforeUpdate: Utils.HashPassword
    },
    instanceMethods: {
      ComparePassword: Utils.ComparePassword
    },
    classMethods: {
      Login: Utils.Login,
      LoginMobile: Utils.LoginMobile,//criferlo
      associate: function(models) {

        Usuario.hasMany(models.Direccion);
        Usuario.hasMany(models.Pedido);
        Usuario.hasMany(models.Producto);
        Usuario.belongsTo(models.Pais);
      }
    }
  });

  return Usuario;
};
