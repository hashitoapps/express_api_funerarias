"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Variedad
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var Variedad = sequelize.define('Variedad', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},		
		Nombre: { type: DataTypes.STRING, allowNull: false}
	}, {
		classMethods: {
			associate: function (models) {
				Variedad.belongsTo(models.SubCategoria);
				Variedad.hasMany(models.Producto);				
			}
		}
	});

	return Variedad;
};				
			