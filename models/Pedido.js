"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Restaurante
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var Pedido = sequelize.define('Pedido', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},		
		Fecha: { type: DataTypes.DATE, allowNull: false},
		//A: abierto C: confirmado E: enviado T: terminado R: rechazado
		Estado: { type: DataTypes.ENUM('A','C','E','T','R'), allowNull: false},
		//Calificaciòn usuario 0-5
		CalificacionUsuario: { type: DataTypes.DECIMAL(1,0), allowNull: true},
		//Comentario despues de recibir pedido
		ComentarioUsuario: { type: DataTypes.STRING, allowNull: true},
		//Observacion al enviar el pedido ej. mas salsa de tomate
		ObservacionUsuario: { type: DataTypes.STRING, allowNull: true},
		//Observacion antes de enviar el pedido por parte de la empresa
		ObservacionEmpresa: { type: DataTypes.STRING, allowNull: true},
	}, {
		classMethods: {
			associate: function (models) {
				Pedido.belongsTo(models.Usuario);
				Pedido.belongsTo(models.MedioPago);
				Pedido.hasMany(models.DetallePedido);
			}
		}
	});

	return Pedido;
};