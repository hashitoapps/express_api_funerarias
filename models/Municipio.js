"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Municipio
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var Municipio = sequelize.define('Municipio', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		Nombre : { type: DataTypes.STRING, allowNull: false},
		Codigo : { type: DataTypes.STRING, allowNull: false},
	}, {
		classMethods: {
			associate: function (models) {
				Municipio.belongsTo(models.Departamento);
				Municipio.hasMany(models.ProductoMunicipio);

			}
		}
	});

	return Municipio;
};
