module.exports = function(apiVersion) {
  var models      = require('../models');
  var express     = require('express');
  var router      = express.Router();
  var controllers = require('../controllers')[apiVersion];

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/productomunicipio', function(req, res) {
    models
      .ProductoMunicipio
      .findAll({
        include:[{
          model: models.Producto
        },
        {
          model: models.Municipio
        }
          ]
      })
      .then(function(result) {
        res.send(result);
      })
      .catch(function(error) {
        console.log(error);
        res.status(500).send(error);
      })
  });

  router.post('/productomunicipio',controllers.ProductoMunicipio.Insert);

  router.delete('/productomunicipio/:Id/deleteall',controllers.ProductoMunicipio.DeleteAll);

  router.get('/productomunicipio/:Id/getbyproduct',controllers.ProductoMunicipio.GetByProduct);

  return router;
};
