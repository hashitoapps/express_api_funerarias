module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/imagenes',
    controllers.Imagen.GetAll);
  router.get('/imagenes/:Id',
    controllers.Imagen.GetById);
  router.post('/imagenes',
    controllers.Imagen.Insert);
  router.put('/imagenes/:Id',
    controllers.Imagen.Update);
  router.delete('/imagenes/:Id',
    controllers.Imagen.Delete);

  return router;
};
