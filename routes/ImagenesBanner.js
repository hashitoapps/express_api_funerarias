module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer el API
   */
  router.get('/imagenesbanner',
    controllers.ImagenesBanner.GetImageRuta);
  router.get('/imagenesbanner/:Id',
    controllers.ImagenesBanner.GetById);
  router.post('/imagenesbanner',
    controllers.ImagenesBanner.Insert);
  router.put('/imagenesbanner/:Id',
    controllers.ImagenesBanner.Update);
  router.delete('/imagenesbanner/:Id',
    controllers.ImagenesBanner.Delete);

  return router;
};
