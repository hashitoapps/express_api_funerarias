module.exports = function(apiVersion) {
  var models      = require('../models');
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
   //llama los departamentos que tienen un idpais determinado
  router.get('/paises', function(req, res) {
    models
      .Pais
      .findAndCountAll({
        
      })
      .then(function(result) {
        res.send(result);
      })
      .catch(function(error) {
        console.log(error);
        res.status(500).send(error);
      })
  });

  return router;
};
